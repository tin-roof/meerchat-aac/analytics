package analytics

import (
	"context"
	"fmt"
	"sort"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type BaseData struct {
	Buttons  []Stat `json:"buttons"`  // date and count
	Phrases  []Stat `json:"phrases"`  // date and count
	Sessions []Stat `json:"sessions"` // date and count
	Totals   struct {
		Actions  int `json:"actions"`
		Boards   int `json:"boards"`
		Buttons  int `json:"buttons"`
		Phrases  int `json:"phrases"`
		Sessions int `json:"sessions"`
	} `json:"totals"`
	TopActions []Stat `json:"topActions"` // title and count
	TopBoards  []Stat `json:"topBoards"`  // id title and count
	TopButtons []Stat `json:"topButtons"` // title and count
}

// Stat defines what could be in a single stat row
type Stat struct {
	Count int        `json:"count"`
	Date  *time.Time `json:"date,omitempty"`
	ID    string     `json:"id,omitempty"`
	Title string     `json:"title,omitempty"`
}

// GetBaseData queries the DB for event data and runs a bunch of calculations to pull out the basic info needed for the initial dashboard load
func GetBaseData(ctx context.Context, db database.DB, profileId uuid.UUID, start time.Time) (*BaseData, error) {
	ctx, log := logger.New(
		ctx,
		"analytics.GetBaseData",
		logger.WithField("profileId", profileId),
	)

	log.Info(ctx, "getting base data")

	data := BaseData{}

	// var wg sync.WaitGroup

	// get button stats
	// wg.Add(1)
	// go func() {
	// 	defer wg.Done()
	data.getButtonStats(ctx, db, profileId, start)
	// }()

	// get counted stats (phrase play events, sessions)
	// wg.Add(1)
	// go func() {
	// 	defer wg.Done()
	data.getCountedStats(ctx, db, profileId, start)
	// }()

	// wait for all the routines to finish
	// wg.Wait()

	return &data, nil
}

// getButtonStats queries for all the button press events and aggregates them into various stats to be used in the dashboard
func (b *BaseData) getButtonStats(ctx context.Context, db database.DB, profileId uuid.UUID, start time.Time) error {
	ctx, log := logger.New(
		ctx,
		"analytics.getButtonStats",
		logger.WithField("profileId", profileId),
	)

	log.Info(ctx, "getting button stats")

	// lookup all the button press events that happened since the start date
	rows, err := db.Conn.Query(
		ctx,
		`SELECT
			a."created_at",
			a."details",
			p."title" as board_title
		FROM
			"analytics" a,
			"page" p
		WHERE
			a."details" ->> 'boardId' = p."id"::string
			AND a."user" = $1
			AND a."type" = 'button_push'
			AND a."created_at" >= $2
		ORDER BY a."created_at" DESC;`,
		profileId,
		start,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "error running button stats query")
		return err
	}

	// create the data maps for counting things
	actionTotals := map[string]int{}
	buttonIDTitleMap := map[string]string{}
	buttonDayTotals := map[string]int{}
	buttonTotals := map[string]int{}
	boardIDTitleMap := map[string]string{}
	boardTotals := map[string]int{}

	// build the family member list
	for rows.Next() {
		var createdAt time.Time
		var details Details
		var boardTitle string
		if err := rows.Scan(
			&createdAt,
			&details,
			&boardTitle,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning button event query row")
			continue
		}

		// handle actions totals
		if len(details.Actions) > 0 {
			for _, action := range details.Actions {
				actn := string(action)
				if _, ok := actionTotals[actn]; !ok {
					actionTotals[actn] = 1
				} else {
					actionTotals[actn]++
				}
			}
		}

		// update button day totals
		btnDayID := createdAt.Format("2006-01-02")
		if _, ok := buttonDayTotals[btnDayID]; !ok {
			buttonDayTotals[btnDayID] = 1
		} else {
			buttonDayTotals[btnDayID]++
		}

		// update button totals
		btnID := fmt.Sprintf("%s:%s", details.BoardID, details.ButtonID)
		if _, ok := buttonTotals[btnID]; !ok {
			buttonTotals[btnID] = 1
		} else {
			buttonTotals[btnID]++
		}

		// add the button label to the map
		if _, ok := buttonIDTitleMap[btnID]; !ok {
			buttonIDTitleMap[btnID] = getButtonTitle(details)
		}

		// update board id to title map
		boardIDStr := details.BoardID.String()
		if _, ok := boardIDTitleMap[boardIDStr]; !ok {
			boardIDTitleMap[boardIDStr] = boardTitle
		}

		// update board totals
		if _, ok := boardTotals[boardIDStr]; !ok {
			boardTotals[boardIDStr] = 1
		} else {
			boardTotals[boardIDStr]++
		}
	}

	// process all the stats now
	b.Buttons = toStats(buttonDayTotals, 31)
	for i, btn := range b.Buttons {
		date, err := time.Parse("2006-01-02", btn.Title)
		if err != nil {
			log.WithError(err).Info(ctx, "error parsing button date")
			continue
		}
		b.Buttons[i].Date = &date
		b.Buttons[i].Title = ""
	}

	b.TopActions = toStats(actionTotals, 4)

	// fill in the titles for the top boards
	b.TopBoards = toStats(boardTotals, 10)
	for i, brd := range b.TopBoards {
		b.TopBoards[i].ID = brd.Title
		b.TopBoards[i].Title = boardIDTitleMap[brd.Title]
	}

	// fill in the titles for the tob buttons
	b.TopButtons = toStats(buttonTotals, 4)
	for i, btn := range b.TopButtons {
		b.TopButtons[i].Title = buttonIDTitleMap[btn.Title]
	}

	// calculate the some totals
	b.Totals.Actions = getTotal(actionTotals)
	b.Totals.Buttons = getTotal(buttonTotals)
	b.Totals.Boards = getTotal(boardTotals)

	return nil
}

// getCountedStats queries for all the (phrase play events, sessions) and aggregates them into various stats to be used in the dashboard
func (b *BaseData) getCountedStats(ctx context.Context, db database.DB, profileId uuid.UUID, start time.Time) error {
	ctx, log := logger.New(
		ctx,
		"analytics.getButtonStats",
		logger.WithField("profileId", profileId),
	)

	log.Info(ctx, "getting button stats")

	// lookup all the phrase play events that happened since the start date
	rows, err := db.Conn.Query(
		ctx,
		`SELECT
			date_trunc('day', a."created_at") as "day",
			COUNT(*) filter (where a."type" = 'phrase_play') as phrase_play,
			COUNT(DISTINCT "session_id") as sessions
		FROM
			"analytics" a
		WHERE
			"user" = $1
			AND "created_at" >= $2
		GROUP BY 1
		ORDER BY "day" DESC;`,
		profileId,
		start,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "error running button stats query")
		return err
	}

	// create the data maps for counting things
	phraseTotal := 0
	sessionsTotal := 0

	// build the phrase stats
	for rows.Next() {
		var createdAt time.Time
		var phraseCount int
		var sessionsCount int
		if err := rows.Scan(
			&createdAt,
			&phraseCount,
			&sessionsCount,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning phrase event query row")
			continue
		}

		// update the total phrase count
		phraseTotal += phraseCount

		// add the state to the data
		b.Phrases = append(b.Phrases, Stat{Date: &createdAt, Count: phraseCount})

		// update the total sessions count
		sessionsTotal += sessionsCount

		b.Sessions = append(b.Sessions, Stat{Date: &createdAt, Count: sessionsCount})
	}

	// set the total
	b.Totals.Phrases = phraseTotal
	b.Totals.Sessions = sessionsTotal

	return nil
}

// --- Helper Functions ---

// toStats returns the top x keys from the data map
func toStats(data map[string]int, topX int) []Stat {
	stats := make([]Stat, 0, len(data))
	for k, v := range data {
		stats = append(stats, Stat{
			Count: v,
			Title: k,
		})
	}

	// sort the stats
	sort.Slice(stats, func(i, j int) bool {
		return stats[i].Count > stats[j].Count
	})

	// trim the stats to the top x
	if len(stats) > topX {
		stats = stats[:topX]
	}

	return stats
}

// getTotal gets the total counts of the map
func getTotal(data map[string]int) int {
	total := 0
	for _, v := range data {
		total += v
	}
	return total
}

// getButtonTitle gets the title of the button
func getButtonTitle(details Details) string {
	// return the label if it is set
	if details.Label != "" {
		return details.Label
	}

	// check the phase expression and use that if it is set
	if details.Phrase != "" {
		return details.Phrase
	}

	return "N/A"
}
