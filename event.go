package analytics

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// EventType defines the possible types of events that can be tracked
type EventType string

const (
	ButtonPushEvent   EventType = "button_push"   // button is pushed
	PhraseClearEvent  EventType = "phrase_clear"  // whole phrase is cleared
	PhraseDeleteEvent EventType = "phrase_delete" // single button is removed from a phrase
	PhrasePlayEvent   EventType = "phrase_play"   // phrase is played
)

// EventAction defines the possible actions that can be taken from an event
type EventAction string

const (
	AddToPhraseAction  EventAction = "add_to_phrase"
	OpenNewBoardAction EventAction = "open_new_board"
	PlayNowAction      EventAction = "play_now"
)

// Event is the struct that is used to track events
type Event struct {
	ID        *uuid.UUID `json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	Details   Details    `json:"details"`
	Profile   *uuid.UUID `json:"user"`
	SessionID *uuid.UUID `json:"sessionId"`
	Type      EventType  `json:"type"`
}

// Details defines the possible details of an event
type Details struct {
	Actions  []EventAction `json:"actions,omitempty"`  // play now, add to phrase, open new board
	BoardID  *uuid.UUID    `json:"boardId,omitempty"`  // id of the board the the button or phrase is on
	ButtonID string        `json:"buttonId,omitempty"` // id of the button that was pushed
	Image    string        `json:"image,omitempty"`    // image on the button if there is one
	Label    string        `json:"label,omitempty"`    // label of the button if there is one
	Link     string        `json:"link,omitempty"`     // link that was opened if there is one
	Phrase   string        `json:"phrase,omitempty"`   // full phrase that was played
}

// Save saves an event to the database
func (e *Event) Save(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(
		ctx,
		"analytics.Event.Save",
	)

	log.Info(ctx, "saving a new event")

	// save the membership
	err := db.Conn.QueryRow(
		ctx,
		`INSERT INTO "analytics" ("type", "user", "details", "session_id") VALUES ($1, $2, $3, $4) RETURNING "id";`,
		e.Type,
		e.Profile,
		e.Details,
		e.SessionID,
	).Scan(&e.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running event save query")
		return err
	}

	return nil
}
